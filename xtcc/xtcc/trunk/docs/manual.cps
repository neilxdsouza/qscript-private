\initial {D}
\entry {Design goals}{5}
\entry {Detailed user manual for each individual program}{12}
\initial {I}
\entry {Installing and building the compiler and associated tools}{9}
\initial {O}
\entry {Overview of MR}{2}
\entry {Overview of XtCC}{1}
\entry {Overview of XtCC Compiler}{11}
\initial {R}
\entry {Requirements}{8}
\initial {S}
\entry {Scope for Computer assistance in DP}{4}
\initial {T}
\entry {Testing suite}{10}
\entry {Tutorial and Reference documentation of a person who wants to get in and work in developing this software}{13}
