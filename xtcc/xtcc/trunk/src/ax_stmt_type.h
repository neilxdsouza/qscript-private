#ifndef AX_STMT_TYPE_H
#define AX_STMT_TYPE_H
namespace Table {

enum axstmt_type { ax_uninit, txt_axstmt, tot_axstmt, cnt_axstmt, fld_axstmt, inc_axstmt };

}

#endif /* AX_STMT_TYPE_H */
