#ifndef XTCC_MEAN_STDDEV_STRUCT_H
#define XTCC_MEAN_STDDEV_STRUCT_H

struct MeanStdDevInc {
	double sum_n;
	double n;
};

#endif /* XTCC_MEAN_STDDEV_STRUCT_H */
