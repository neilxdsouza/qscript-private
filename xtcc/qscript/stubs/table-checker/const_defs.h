/*
 *  xtcc/xtcc/qscript/stubs/simple_compiler/const_defs.h
 *
 *  Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009 Neil Xavier D'Souza
 */
#ifndef MY_CONST_DEFS_H
#define MY_CONST_DEFS_H

#define MY_STR_MAX 4095
#define MAX_RANGE_ELEMENTS 4095

#endif /* MY__CONST_DEFS_H */
