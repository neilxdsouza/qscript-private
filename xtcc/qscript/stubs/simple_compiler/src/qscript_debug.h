#ifndef qscript_debug_h
#define qscript_debug_h

#include <inttypes.h>
#include <sys/types.h>
namespace qscript_debug{
	extern const int32_t DEBUG_UnaryExpression;
	extern const int32_t DEBUG_Unary2Expression;
	extern const int32_t DEBUG_BinaryExpression;
	extern const int32_t DEBUG_Binary2Expression;
	extern const int32_t DEBUG_DeclarationStatement;
	extern const int32_t DEBUG_qscript_parser;
	extern const int32_t DEBUG_RangeQuestion;
	extern const int32_t DEBUG_NamedStubQuestion;
	extern const int32_t MAINTAINER_MESSAGES;
	extern const int32_t DEBUG_LoadData;
	extern const int32_t DEBUG_IfStatement;
	extern const int32_t DEBUG_ForStatement;
	extern const int32_t DEBUG_CompoundStatement;
}


#endif /* qscript_debug_h */
