#ifndef code_gen_utils_h
#define code_gen_utils_h

#include <sstream>
#include "compiled_code.h"
#include "xtcc_set.h"

void PrintTemporaryXtccSet(ExpressionCompiledCode &code, XtccSet * xs);

#endif /* code_gen_utils_h */
